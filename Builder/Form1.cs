﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Builder
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {

            
            IMenueBuilder builder = null;

            if (radioButton1.Checked)
                builder = new HappyMealBuilder(HMHaupt.Burger, HMBeilage.Pommes, HMSaft.Apfel, HMNach.Fruchtpüree);
            else 
                builder = new PlusMenueBuilder(PMHaupt.Chickenburger, PMBeilage.Salat, PMSaft.Cola);

            MenueDirector md = new MenueDirector(builder);
            md.BestellungAufgeben();
            MessageBox.Show(md.GetMenue().ToString());
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Builder
{
    public enum HMHaupt
    {
        Burger,
        Nuggets
    }
    public enum HMBeilage
    {
        Pommes,
        Salat
    }
    public enum HMSaft
    {
        Apfel,
        Orange
    }
    public enum HMNach
    {
        Jogurt,
        Fruchtpüree
    }

    public class HappyMealBuilder : MenueBuilder
    {
        public HappyMealBuilder(HMHaupt haupt, HMBeilage beilage, HMSaft saft, HMNach nach)
        {
            produkte.Add(new Product() { Bezeichnung = haupt.ToString("g") });
            produkte.Add(new Product() { Bezeichnung = beilage.ToString("g") });
            produkte.Add(new Product() { Bezeichnung = saft.ToString("g") });
            produkte.Add(new Product() { Bezeichnung = nach.ToString("g") });
        }
    }
}

﻿using System.Collections.Generic;
using System.Linq;

namespace Builder
{
    public class Menue
    {
        List<Product> items = new List<Product>();
        public void AddItem(Product item)
        {
            items.Add(item);
        }

        public override string ToString()
        {
            var joined = from item in items
                         select item.Bezeichnung;

            return string.Join(" ", joined);
        }
    }
}
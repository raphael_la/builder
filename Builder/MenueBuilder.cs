﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Builder
{
    public class MenueBuilder : IMenueBuilder
    {
        private Menue menue = new Menue();
        protected List<Product> produkte = new List<Product>();

        public void Build()
        {
            foreach(var item in produkte)
            {
                menue.AddItem(item);
            }
        }

        public Menue Tablett()
        {
            return menue;
        }
    }
}

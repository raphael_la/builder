﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Builder
{
    public class MenueDirector
    {
        IMenueBuilder menueBuilder;
        public MenueDirector(IMenueBuilder builder)
        {
            menueBuilder = builder;
        }

        public void BestellungAufgeben()
        {
            menueBuilder.Build();
        }

        public Menue GetMenue()
        {
            return menueBuilder.Tablett();
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Builder
{
    public enum PMHaupt
    {
        Chickenburger,
        Wrap
    }
    public enum PMBeilage
    {
        Wedges,
        Salat
    }
    public enum PMSaft
    {
        Cola,
        Sprite
    }

    public class PlusMenueBuilder : MenueBuilder
    {
        public PlusMenueBuilder(PMHaupt haupt, PMBeilage beilage, PMSaft saft)
        {
            produkte.Add(new Product() { Bezeichnung = haupt.ToString("g") });
            produkte.Add(new Product() { Bezeichnung = beilage.ToString("g") });
            produkte.Add(new Product() { Bezeichnung = saft.ToString("g") });
        }  
    }
}
